// types/gadwal.d.ts
declare module 'gadwal' {
  import { FC, HTMLAttributes } from 'react';

  export interface TableHeaderProps extends HTMLAttributes<HTMLDivElement> {}
  export interface TableBodyProps extends HTMLAttributes<HTMLDivElement> {}
  export interface TableRowCellProps extends HTMLAttributes<HTMLDivElement> {}
  export interface TableHeaderCellProps extends HTMLAttributes<HTMLDivElement> {}

  export interface TableRow {
    name: string;
    size: number;
    custom?: (data: any , index?: number | string) => any;
    header: any;
    key?: string | number;
  }


  export interface GadwalRow<T> extends TableRow {
    name: Extract<keyof T, string> | "custom";
    custom? : (data : T , index?: number | string) => any
  }

  export interface TableProps {
    data: any[];
    table: TableRow[];
    headerProps?: TableHeaderProps;
    bodyProps?: TableBodyProps;
    stripped?: boolean;
    animated?: boolean;
    fixedHeight?: boolean;
    headerCellProps?: TableHeaderCellProps;
    bodyCellProps?: TableRowCellProps;
    className?: string;
  }

  export interface TableHeaderRow {
    header: any;
    key: string | number;
    size: number;
    custom?: (data: any , index?: number | string) => any;
    name: string;
  }

  const Table: FC<TableProps>;

  export default Table;
}
