# بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيمِ

# Gadwal


![Funny GIF](https://media1.giphy.com/media/v1.Y2lkPTc5MGI3NjExMmxmcDQ0ZmRmajg2Y2s2M3h3eGtobHdsd2puZ2Y0cXJpbWQxNThxOCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/yE6hqAVMno6gvmQuIs/giphy.gif)


Basic React Table.


## Features

- Customizable columns
- Supports custom rendering for each cell
- Tailwind CSS support

## Installation

```bash
npm install gadwal
```

## Add styles to main.tsx

```tsx
import "gadwal/dist/style.css"
```

# Usage

Sample Data

```js
const data = [
  {
    id: 1,
    name: "Leanne Graham",
    username: "Bret",
    email: "oqkz9@example.com",
    website: "hildegard.org",
    phone: "1-770-736-8031 x56442",
    company: {
      name: "Romaguera-Crona",
      catchPhrase: "Multi-layered client-server neural-net",
      bs: "harness real-time e-markets",
    },
  },
  {
    id: 2,
    name: "Ervin Howell",
    username: "Antonette",
    email: "vOy9C@example.com",
    website: "anastasia.net",
    phone: "010-692-6593 x09125",
    company: {
      name: "Deckow-Crist",
      catchPhrase: "Proactive didactic contingency",
      bs: "synergize scalable supply-chains",
    },
  },
];
```

# Table Configuration

```js
const table: TableRow[] = [
  { header: "id", name: "id", size: 2 },
  { header: "name", name: "name", size: 5 },
  { header: "email", name: "email", size: 5 },
  { header: "website", name: "website", size: 4 },
  { header: "phone", name: "phone", size: 4 },
  { header: "company", name: "company", size: 4, custom: (d) => d.company.name },
];
```


# Component Usage

```tsx
import React from 'react';
import Table from 'gadwal';

const data = [
  {
    id: 1,
    name: "Leanne Graham",
    username: "Bret",
    email: "oqkz9@example.com",
    website: "hildegard.org",
    phone: "1-770-736-8031 x56442",
    company: {
      name: "Romaguera-Crona",
      catchPhrase: "Multi-layered client-server neural-net",
      bs: "harness real-time e-markets",
    },
  },
  {
    id: 2,
    name: "Ervin Howell",
    username: "Antonette",
    email: "vOy9C@example.com",
    website: "anastasia.net",
    phone: "010-692-6593 x09125",
    company: {
      name: "Deckow-Crist",
      catchPhrase: "Proactive didactic contingency",
      bs: "synergize scalable supply-chains",
    },
  },
];

const table = [
  { header: "id", name: "id", size: 2 },
  { header: "name", name: "name", size: 5 },
  { header: "email", name: "email", size: 5 },
  { header: "website", name: "website", size: 4 },
  { header: "phone", name: "phone", size: 4 },
  { header: "company", name: "company", size: 4, custom: (d) => d.company.name },
];

function App() {
  return (
    <div className="flex w-full">
      <Table data={data} table={table} />
    </div>
  );
}

export default App;
```
## props

| props             | usage                                                              |required   | default |
| ----------------- | ------------------------------------------------------------------ | --------- |---------|
| data | table data that is going to be rendred| true | -|
| table | coulums of the table each represents a cell | true| - |
| fixedHeight | to force each table row to a fixed height (50px) | false | true |
| animated | animate table content | false | false |
| stripped  | make table rows stripped  | false | true |
| bodyProps | to pass custom styles , classes , ...etc all div attributes to each row of data | false | {} |
| headerProps | to pass custom styles , classes , ...etc all div attributes to only table head  | false | {} |
| bodyCellProps | to pass custom styles , classes , ...etc all div attributes to each row cells of data | false | {} |
| headerCellProps | to pass custom styles , classes , ...etc all div attributes to only table head cells  | false | {} |

## License

[MIT](https://choosealicense.com/licenses/mit/)


## 🚀 About Me
I'm a MERN stack developer...


## Authors

- [@abdoseadaa](https://x.com/abdoseadaa) 

## Repository

[Repository](https://gitlab.com/abdom.seada/gadwal)


## Summary

﴾ ذَٰلِكَ فَضْلُ اللَّهِ يُؤْتِيهِ مَن يَشَاءُ ۚ وَاللَّهُ ذُو الْفَضْلِ الْعَظِيمِ﴿
