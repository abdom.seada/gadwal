export interface TableHeaderProps extends React.HTMLAttributes<HTMLDivElement> {}
export interface TableBodyProps extends React.HTMLAttributes<HTMLDivElement> {}

export interface TableRow {
    name : string ;
    size : number ;
    custom?: (data : any , index?: number | string)  => any
    header : any
    key? : string | number
  }
  
export interface TableProps {
    data : any[],
    table : TableRow[]
    headerProps?: TableHeaderProps
    bodyProps?: TableBodyProps  
    stripped?: boolean
    animated?: boolean
    fixedHeight?: boolean
    className?: string
}
  
export interface TableHeaderRow {
    header : any
    key : string | number
    size : number
    custom?: (data : any , index?: number | string)  => any
    name : string
}