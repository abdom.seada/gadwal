// import { TableProps } from './table.types'
import useTable from './hooks/useTable'
import TableHeader from './components/TableHeader'
import "./styles/table.grid.css"
import TableBody from './components/TableBody'
import "./styles/table.styles.css"
import { TableProps } from 'gadwal'
import DataIsNotArray from './components/DataIsNotArray'

export default function Table(props : TableProps) {

  // console.log(Array.isArray(props.data));
  

  if(props.data && !Array.isArray(props.data)) return <DataIsNotArray data={props.data} />


  const { headerData , bodyData } = useTable(props)
    
  return (

      <div className={`w-full ${props?.className || ""}`} data-gadwal-table>

        <TableHeader  table={headerData} headerProps={props?.headerProps}  headerCellProps={props?.headerCellProps}  />
        <TableBody  
              data={bodyData}  
              table={headerData}   
              bodyProps={props?.bodyProps} 
              stripped={props?.stripped ?? true}
              animated={props?.animated ?? false}
              fixedHeight={props?.fixedHeight ?? true}
              bodyCellProps={props?.bodyCellProps}
              
              
              />
      </div> 

  )
}
