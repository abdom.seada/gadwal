
export default function DataIsNotArray({data} : any) {
    const dataString = JSON.stringify(data)
  return (
    <div className="w-[500px] break-before-all border-[1px] rounded-md bg-[#EEEDEB] mx-auto my-5">
        <h1 className="text-[#E9C46A] font-bold text-[36px] border-b-[1px] p-2">Gadwal Error</h1>
        <h2 className="bg-[#2A9D8F] text-[#F4F3F0] font-bold text-[18px]  p-2"> data expected to be an array but got </h2>
        <div className="text-[#E76F51] bg-[#F4F3F0]  p-2 text-[18px] break-words max-h-[300px] overflow-auto">
            {dataString}
        </div>
    </div>
  )
}
