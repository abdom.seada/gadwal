// import { TableHeaderProps, TableHeaderRow } from '../table.types';

import { TableHeaderProps, TableHeaderRow , TableHeaderCellProps} from "gadwal"

interface Props  {
   table : TableHeaderRow[],
   headerProps?: TableHeaderProps
   headerCellProps?: TableHeaderCellProps
}


export default function TableHeader(props : Props) {

  const {table} = props

  const className = (head : TableHeaderRow) => `gadwal-header-cell span-${head.size} bg-[#E5E7EB] ${props.headerCellProps?.className || ""}`
  return (
    <div {...props.headerProps} className={`flex w-full ${props.headerProps?.className || ""}`} >
        {
          table.map( (head) =>{
            return <div {...props.headerCellProps} className={ className(head) }   key={head.key}  >
                 {head.header}
            </div>
          })
        }
    </div>
  )
}
