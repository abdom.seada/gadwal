import {
  TableBodyProps,
  TableHeaderRow,
  TableRow,
  TableRowCellProps,
} from "gadwal";

interface Props {
  data: TableRow[];
  table: TableHeaderRow[];
  stripped: boolean;
  animated: boolean;
  fixedHeight: boolean;
  bodyProps?: TableBodyProps;
  bodyCellProps?: TableRowCellProps;
}

interface DataRow {
  [key: string]: any;
}

export default function TableBody(props: Props) {
  const { table } = props;

  return (
    props?.data &&
    props?.data?.map((dataRow: DataRow, rowIndex) => {
      const isEven = rowIndex % 2 === 0;

      return (
        <div
          key={dataRow.key}
          {...props?.bodyProps}
          className={`flex ${props.stripped && isEven ? "bg-gray-100" : ""}   ${
            props.animated ? "animate-table-row" : ""
          }  ${props.bodyProps?.className}`}
          style={
            props.animated ? { animationDelay: `${rowIndex * 0.1}s` } : {}
          }>
          {table?.map((row) => {
            const className = `span-${row.size} gadwal-body-cell  ${
              props.fixedHeight ? "h-[50px] whitespace-nowrap truncate" : ""
            } ${props.bodyCellProps?.className || ""}`;

            if (row.custom) {
              return (
                <div
                  {...props?.bodyCellProps}
                  className={className}
                  key={row.key}>
                  {row.custom(dataRow, rowIndex + 1)}
                </div>
              );
            }

            return (
              <div
                {...props?.bodyCellProps}
                className={className}
                key={row.key}>
                {dataRow[row.name]}
              </div>
            );
          })}
        </div>
      );
    })
  );
}
