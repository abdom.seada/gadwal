import { TableRow } from "gadwal"
import Table from "./components/table/Table"


function App() {

  const data = [
    {
      id: 1,
      name: "Leanne Graham",
      username: "Bret",
      email: "oqKz9@example.com",
      website: "hildegard.org",
      phone: "1-770-736-8031 x56442",
      company: {
        name: "Romaguera-Crona",
        catchPhrase: "Multi-layered client-server neural-net",
        bs: "harness real-time e-markets",
      },
    }
   
  ]

  const table : TableRow[] = [
      { header : <>hello</>, name : "id" , size : 2 ,  },
      { header : "name", name : "name" , size : 5 ,  },
      { header : "email", name : "email" , size : 5 },
      { header : "website", name : "website" , size : 4 ,  },
      { header : "phone", name : "phone" , size : 4 ,  },
      { header : "company", name : "company" , size : 4 , custom : (d) => d.company.name },
  ]



  return (
   <div className="  w-full overflow-auto">
     

     Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perferendis, quibusdam optio saepe illo numquam dolore, doloribus, alias harum ratione in deleniti ea magni officiis expedita. Ipsum veniam ipsa pariatur quas ab asperiores modi, temporibus labore blanditiis quaerat aspernatur alias. Reprehenderit corrupti, rem at nobis deleniti nam, odio est magnam soluta voluptate dolores. Velit quaerat distinctio eos tenetur maxime magni quibusdam, dicta dignissimos perspiciatis odit error placeat veritatis quia voluptate assumenda similique recusandae autem voluptatem nam neque necessitatibus facilis atque? Atque repellendus excepturi rerum illum consequatur cupiditate temporibus ipsam. Optio fugit rem tempore voluptates. Porro velit facere dignissimos aliquid, nam adipisci.


     <div className="flex my-4 gap-3">
      <div className="w-1/2 text-justify bg-amber-50 p-2">
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate, rem?
      </div>

      <div className="w-1/2 text-justify bg-amber-50 p-2">
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto delectus doloremque quia molestias iste, eveniet quisquam adipisci! Laborum fugit atque laudantium quas facere, sapiente iusto pariatur, velit temporibus rerum quibusdam quia veritatis tempore id et nemo blanditiis repellat a ab obcaecati commodi? Maiores commodi voluptates dicta. Odit commodi sapiente rerum ad illo? Aspernatur eveniet nemo consectetur. Dicta consectetur consequatur nisi deleniti fuga sunt harum quos dolores. Fugit maxime adipisci laudantium sit amet tenetur! Labore repellat cupiditate consequatur quisquam eligendi aut voluptas itaque distinctio voluptatibus veniam. Amet nam cum vel consequuntur cumque eius sequi, nobis dolorem quis deserunt est quos obcaecati!
      </div>
     </div>


      <div className="overflow-x-auto">
        <Table data={data} table={table}  className="min-w-[1000px]"/> 
      </div>
   </div>
  )
}

export default App
